@extends('layout.app')

    @section('content')
    <div class="container-fluid">

    <div class="x_content">
        <form>
            <div class="row">

              <div class="col-xs-12 col-sm-12 col-md-12">
                 <input type="hidden" name="_token" value="0d9bG9rwJM7BdmLsBE2kpKHOlxNsFGm7FDA57tSG">
              </div>

                <div class="col-md-4 col-sm-12  form-group">
                    <select  class="form-control barang" id="jenis-barang" name="jenis-barang" class="form-control">
                        <option value="0" disabled="true" selected="true">--Pilih Jenis Barang--</option>
                        <option value="1">1. Sekolah</option>
                        <option value="2">2. Rumah Tangga</option>
                        <option value="3">3. IT</option>
                        <option value="4">4. Sarana dan Prasarana</option>
                    </select><br>

                <select  class="form-control " id="" name="" class="form-control">
                    <option value="0" disabled="true" selected="true">--Pilih Status Barang--</option>
                    <option value="1"> Yayasan</option>
                    <option value="2"> TKIT 1 AL ISTIQOMAH</option>
                    <option value="3"> TKIT 2 AL ISTIQOMAH</option>
                    <option value="4"> MI AL-ISTIQOMAH</option>
                    <option value="5"> SDIT 1 AL ISTIQOMAH</option>
                    <option value="6"> SDIT 2 AL ISTIQOMAH</option>
                    <option value="7"> MI MODEL AL ISTIQOMAH</option>
                </select>
                </div>

                <div class="col-md-4 col-sm-12  form-group">
                    <select  class="form-control anggaran" id="jenis-anggaran" name="jenis-anggaran" class="form-control">
                        <option value="0" disabled="true" selected="true">--Pilih Jenis Anggaran--</option>
                        <option value="1"> Dana BOS</option>
                        <option value="2"> Yayasan</option>
                        <option value="3"> Unit</option>
                    </select><br>

                    <select  class="form-control tingkatan" id="periode" name="periode">
                        <option value="0" disabled="true" selected="true">--Pilih Periode Tahun Anggaran--</option>
                        <option value="1415"> 2014/2015</option>
                        <option value="1516"> 2015/2016</option>
                        <option value="1617"> 2016/2017</option>
                        <option value="1718"> 2017/2018</option>
                        <option value="1819"> 2018/2019</option>
                        <option value="1920"> 2019/2020</option>
                        <option value="2021"> 2020/2021</option>
                        <option value="2122"> 2021/2022</option>
                        <option value="2223"> 2022/2023</option>
                    </select>
                    </div>

                <div class="col-md-4 col-sm-12  form-group">
                    <select  class="form-control kelasName" id="id_kelas_show" name="id_kelas_show">
                        <option value="0" disabled="true" selected="true">--Pilih Lokasi (Spesifik)--</option>
                        <option value="1"> TKIT 1 AL ISTIQOMAH</option>
                        <option value="2"> TKIT 2 AL ISTIQOMAH</option>
                        <option value="3"> MI AL-ISTIQOMAH</option>
                        <option value="4"> SDIT 1 AL ISTIQOMAH</option>
                        <option value="5"> SDIT 2 AL ISTIQOMAH</option>
                        <option value="6"> Rakha</option>
                        <option value="7"> TU SDIT 1</option>
                        <option value="8"> Makhtab</option>
                    </select>
            </div> <!--end x_row-->
        </form>

            <div class="ln_solid">
                <div class="table-responsive">
                <table id="dataTable_tagihan" class="table table-striped ">
                    <thead>
                    <tr class="headings"></tr>
                    </thead>
                </table>
                    <input type="hidden" name="_token" value="0d9bG9rwJM7BdmLsBE2kpKHOlxNsFGm7FDA57tSG">
                </div>
            <div class="ln_solid"></div>
        <div class="item form-group">

            <div class="col-md-auto col-sm-auto ">
            <button type="button" class="btn btn-dark btn-sm " style="margin-right: 5px;" onclick="CariDataTagihan();" ><i class="fa fa-search"></i> Lihat Data Barang</button>

            </div>
            <br>
          </div>
        </div>
    </div>
    </div>
@endsection
