@extends('layout.app')
@section('content')

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">Data Pengajuan Aset</h1>
                        <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
                            For more information about DataTables, please visit the <a target="_blank"
                                href="https://datatables.net">official DataTables documentation</a>.</p>
                    {{-- start modal --}}
                    <div class="d-sm-flex align-items-center judul-modal">
                    <div class="judul-modal m-auto">
                    <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#exampleModal" data-whatever="">Buat Pengajuan</button>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Form Pengajuan Aset</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                              <div class="row">
                                <div class="modal-body">
                              <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Nama:</label>
                                <input type="text" class="form-control" id="recipient-name">
                              </div>
                              <div class="form-group">
                                <label for="message-text" class="col-form-label">Bagian:</label>
                                <textarea class="form-control" id="message-text"></textarea>
                              </div>
                              <div class="form-group">
                                <label for="message-text" class="col-form-label">Tahun Anggaran:</label>
                                <textarea class="form-control" id="message-text"></textarea>
                              </div>
                              <div class="form-group">
                                <label for="message-text" class="col-form-label">Jenis Anggaran:</label>
                                <textarea class="form-control" id="message-text"></textarea>
                              </div>
                              <div class="form-group">
                                <label for="message-text" class="col-form-label">Lokasi:</label>
                                <textarea class="form-control" id="message-text"></textarea>
                              </div>
                                </div>
                            </div>
                              {{-- start modal buku paket --}}
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Cari Barang:</label>
                                    <div class="input-group">
                                        <input id="inputgo_nama" name="inputgo_nama" type="text" class="typeahead form-control"><ul class="typeahead dropdown-menu" role="listbox" style="top: 43px; left: 0px; display: none;"><li class="active"><a class="dropdown-item" href="#" role="option"><strong>Bahasa Arab 2</strong></a></li></ul>
                                        <span class="input-group-btn">
                                            <button type="button" id="go_nama" name="go_nama" class="btn btn-primary">Go!</button>
                                        </span>
                                    </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Kode Barang/Barcode:</label>
                                    <input type="text" class="form-control" id="id-barang" name="id_barang" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Kode Barang/Barcode:</label>
                                    <input type="text" class="form-control" id="kode-barang" name="kode_barang" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Kode Produksi Barang:</label>
                                    <input type="text" class="form-control" id="kode-produksi-barang" name="kode_produksi_barang" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Nama barang:</label>
                                    <input type="text" class="form-control" id="nama-barang" name="nama_barang" readonly="">
                                </div>
                            </div>
                              </div>
                            </div>
                            {{-- end modal buku paket --}}
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Send message</button>
                          </div>
                        </div>
                        </div>
                      </div>
                    </div>
                </div>
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Id Pengajuan</th>
                                            <th>Judul Pengajuan</th>
                                            <th>Bagian</th>
                                            <th>Nama Yang Mengajukan</th>
                                            <th>Tahun Anggaran</th>
                                            <th>Lokasi</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Id Pengajuan</th>
                                            <th>Judul Pengajuan</th>
                                            <th>Bagian</th>
                                            <th>Nama Yang Mengajukan</th>
                                            <th>Tahun Anggaran</th>
                                            <th>Lokasi</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td><a class="btn btn-dark">Action</a></td>
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>Accountant</td>
                                            <td>Tokyo</td>
                                            <td>63</td>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
                <!-- /.container-fluid -->
            <!-- End of Main Content -->
@endsection
