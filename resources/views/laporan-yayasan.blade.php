@extends('layout.app')

    @section('content')
    <div class="modal fade"  id="Modal_view_proses" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Proses Validasi</h4>
          <h5> Pehatikan <small>Dengan mengklik Tombol "SIMPAN" maka status surat permohon akan berubah.</small></h5>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" id="demo-form2" >
            <input type="hidden" name="_token" value="0d9bG9rwJM7BdmLsBE2kpKHOlxNsFGm7FDA57tSG">
            <div class="box-body">

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edit_id_permohonan ">Id Permohoanan<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id=edit_id_permohonan name=edit_id_permohonan required="required"  class="form-control col-md-7 col-xs-12" readonly="true">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edit_nomor_induk ">Nomor indduk<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id=edit_nomor_induk name=edit_nomor_induk required="required"  class="form-control col-md-7 col-xs-12" readonly="true">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edit_nama_siswa ">Nama Siswa<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id=edit_nama_siswa name=edit_nama_siswa required="required"  class="form-control col-md-7 col-xs-12" readonly="true">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edit_besaran ">Besaran<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id=edit_besaran name=edit_besaran required="required"  class="form-control col-md-7 col-xs-12" readonly="true">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-sm-3  control-label">Silakan diceklis</label><div class="col-md-9 col-sm-9 ">
                <div class="checkbox">
                <label><input type="checkbox" id="check_kepsek" value="verified_kepsek"> Kepsek Mengetahui dan Ditanda tangani</label>
                </div>
                <div class="checkbox">
                <label><input type="checkbox" id="check_yayasan" value="verified_YPI"> Telah Ditanda Tangani pihak YAYASAN (bagi tunggakan besar)</label>
                </div>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post_deskripsi2">Pesan Untuk Orang Tua<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea id="edit_catatan" required="required" name="edit_catatan" class="form-control col-md-7 col-xs-12"></textarea>
                </div>
              </div>
    @endsection
