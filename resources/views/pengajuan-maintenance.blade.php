@extends('layout.app')
@section('content')
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Maintenance Aset</h1>
                    <p class="mb-2">DataTables is a third party plugin that is used to generate the demo table below.
                        For more information about DataTables, please visit the <a target="_blank"
                            href="https://datatables.net">official DataTables documentation</a>.</p>

                    <!-- DataTales Maintenance Aset -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">DataTables Maintenance Aset</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Jenis</th>
                                            <th>Lokasi</th>
                                            <th>Keterangan</th>
                                            <th>Tanggal</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Jenis</th>
                                            <th>Lokasi</th>
                                            <th>Keterangan</th>
                                            <th>Tanggal</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr>
                                            <td>Printer</td>
                                            <td>IT</td>
                                            <td>SDIT 1</td>
                                            <td>Tidak Bisa Print</td>
                                            <td>2011/04/25</td>
                                            <td>Sudah Selesai</td>
                                            <td><a href="" class="btn btn-dark">Action</a></td>
                                        </tr>
                                        <tr>
                                            <td>Printer</td>
                                            <td>IT</td>
                                            <td>SDIT 1</td>
                                            <td>Tidak Bisa Print</td>
                                            <td>2011/04/25</td>
                                            <td>Sudah Selesai</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-dark" data-toggle="dropdown">Action
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li>
                                                            <a onclick="tampilModal_edit('1','lailipus pitaningrum','azarallesmana78@gmail.com','09','1','2122','0')" href="">
                                                                Edit
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a onclick="return confirm('Akan mengakibatkan beberapa perubahan yang tertaut dengan ID tersebut Anda yakin ingin menghapus ? ( 1 ) Yang bernama (lailipus pitaningrum)')" href="delete_siswa/1">Hapus</a>
                                                        </li>
                                                        <li>
                                                            <a onclick="aktif_siswa('1');" href="#">Aktifkan</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            <!-- End of Main Content -->
@endsection
