-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 09, 2022 at 04:19 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_22_143924_create_notes_table', 2),
(5, '2020_10_22_144046_create_subjects_table', 2),
(6, '2020_11_23_124053_add_column_to_users_table', 3),
(7, '2020_11_23_144355_create_post_table', 4),
(8, '2021_05_23_105316_create_perpustakaan_table', 5),
(9, '2021_05_23_105820_create_arsip_table', 5),
(10, '2021_05_23_114513_create_profil_table', 5),
(11, '2021_05_23_114754_create_renstra_table', 5),
(12, '2021_05_23_114854_create_renkerta_table', 5),
(13, '2021_05_23_115031_create_lakip_table', 5),
(14, '2021_05_23_115307_create_pengadaan_table', 5),
(15, '2021_09_18_141545_create_kontak_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `subject_id`, `title`, `description`, `created_at`, `updated_at`, `slug`) VALUES
(1, NULL, 'X RPL 1', 'PHP DASAR', '2021-11-02 08:40:44', '2021-11-06 04:17:33', NULL),
(2, NULL, 'XI TPMI 2', 'PHP', '2021-11-04 13:51:19', '2021-11-04 13:51:19', NULL),
(3, NULL, 'X TGB 3', 'GAMBAR', '2021-11-04 22:05:49', '2021-11-04 22:05:49', NULL),
(4, NULL, 'XI OTOMASI 2', 'LISTRIK', '2021-11-04 22:08:04', '2021-11-04 22:08:04', NULL),
(5, NULL, 'XII INSTALASI 1', 'KABEL LISTRIK', '2021-11-04 22:14:10', '2021-11-04 22:14:10', NULL),
(6, 1, 'Bismillah', 'Harus semangat', '2021-12-02 03:00:27', '2021-12-02 03:00:27', 'bismillah'),
(7, 2, 'ASDASD', 'ASDASDQWE', '2021-12-02 03:05:19', '2021-12-02 03:05:19', 'asdasd');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kategori` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `nama`, `kategori`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'commodi-quis-laborio', 'Pemerintahan', 'Veniam per', '2020-10-22 13:49:40', '2021-09-18 08:32:41'),
(2, 'alias-eum-et-consequ', 'Perusahaan', 'Commodi quis laborio', '2020-10-22 13:50:28', '2021-09-18 08:33:11'),
(15, 'Commodi quis laborio', 'Pemerintahan', 'Ambar', '2020-11-24 08:50:05', '2021-09-18 08:33:26'),
(20, 'harum-exercitationem', 'Perusahaan', 'Nanda', '2020-11-25 04:34:07', '2021-09-18 08:33:38'),
(21, 'Commodi quis laborio', NULL, 'Beneran', '2021-04-06 01:36:47', '2021-04-25 03:31:07'),
(22, 'velit-anim-esse-est', NULL, 'Commodi quis laborio', '2021-04-15 16:30:11', '2021-04-25 03:44:18'),
(23, 'sit-ut-numquam-earu', NULL, 'Commodi quis laborio', '2021-04-15 16:30:35', '2021-04-25 03:46:13'),
(24, 'alias-eum-et-consequ', NULL, 'Nanda', '2021-04-25 03:31:42', '2021-04-25 03:31:42'),
(25, 'cerita-1', NULL, 'tes cerita', '2021-09-08 13:27:30', '2021-09-08 13:27:30'),
(26, 'Sistem Pengajuan', 'Pemerintahan', '/uploads/images/project/Dzakwan_1631702426.png', '2021-09-15 10:40:26', '2021-09-18 08:33:51');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Laravel', 'Laravel', '2020-10-21 17:00:00', '2020-10-21 17:00:00'),
(2, 'Vue', 'Vue', '2020-10-21 17:00:00', '2020-10-21 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '/uploads/images/profile/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `role`, `image`, `email_verified_at`, `password`, `remember_token`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'Dzakwan', 'dzakwan', 'admin@admin.com', 'admin', '/uploads/images/profile/Dzakwan_1606300468.jpeg', NULL, '$2y$10$8eBuhFN5vlgPdtQl0VL9z.x5ozUT.rqdw0nE8Dw7rduumEb/rk6de', NULL, '2022-01-09 10:51:10', '2020-10-01 21:26:07', '2022-01-09 03:51:10'),
(2, 'Shinta', 'shinta', 'shinta@admin.com', 'admin', '/uploads/images/profile/default.png', NULL, '$2y$10$S8eoSneEkERDCnOlEMl1IeCaZMw3V4HF6W/upRsO8F.mySAoGp/Py', NULL, NULL, '2020-10-04 06:40:03', '2021-09-09 01:23:59'),
(3, 'Riski', 'riski', 'riski@admin.com', 'admin', '/uploads/images/profile/default.png', NULL, '$2y$10$8/yhNvxSAF6PJcxe4R4y3OJw6IFbpFpXtrptO.Sw9yj6lVP/3CUUW', NULL, NULL, '2020-11-24 07:50:11', '2021-09-09 01:24:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
