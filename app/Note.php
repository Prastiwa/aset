<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    // protected $table = 'barang';
    protected $fillable = [
        'title',
        'slug',
        'description'
    ];
}
