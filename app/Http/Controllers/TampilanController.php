<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TampilanController extends Controller
{
    //tampilan
    public function index() {
        // if(Auth::user()->role == 'user') {
        //     Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
        //     return redirect()->to('/dashboard');
        // }


        // $note = Note::all();
        // $no = 1;
        return view('welcome');
    }

    public function user() {
        return view('data-user');
    }

    public function barang() {
        return view('data-barang');
    }

    public function maintenance() {
        return view('pengajuan-maintenance');
    }

    public function aset() {
        return view('pengajuan-aset');
    }
}
